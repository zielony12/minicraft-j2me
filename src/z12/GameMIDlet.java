package z12;

import javax.microedition.midlet.*;
import javax.microedition.lcdui.*;

import z12.Game;

public class GameMIDlet extends MIDlet implements CommandListener {
	private Game game;
	private Display display;
	private Form license;
	private Command accept;
	private Command exit;
	
	public void startApp() {
		display = Display.getDisplay(this);
		license = new Form("License");
		license.append("BSD 4-Clause License\n\nCopyright (c) 2024, zielony12 <karol(at)linux(dot)pl>\nAll rights reserved.\n\nRedistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:\n\n1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.\n\n2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.\n\n3. All advertising materials mentioning features or use of this software must display the following acknowledgement: This product includes software developed by zielony12 <karol(at)linux(dot)pl>.\n\n4. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.\n\nTHIS SOFTWARE IS PROVIDED BY COPYRIGHT HOLDER \"AS IS\" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL COPYRIGHT HOLDER BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.");
		accept = new Command("Accept", Command.SCREEN, 0);
		exit = new Command("Exit", Command.EXIT, 1);
		display.setCurrent(license);
		license.addCommand(accept);
		license.addCommand(exit);
		license.setCommandListener(this);
	}

	public void commandAction(Command c, Displayable d) {
		if(d != license)
			return;
		if(c == accept) {
			goOn();
		} else if(c == exit) {
			notifyDestroyed();
		}
	}

	private void goOn() {
		game = new Game(display);
		game.start();
		display.setCurrent(game);
	}

	public void pauseApp() {
	}

	public void destroyApp(boolean unconditional) {
		if(game != null) {
			game.stop();
			game.cleanUp();
			game = null;
		}
		System.gc();
	}
}