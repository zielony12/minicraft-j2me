package z12;

import javax.microedition.lcdui.game.GameCanvas;

public class InputHandler {
	private int keyCounter = 0;
	private Game game;

	public class Key {
		public int presses, absorbs;
		public boolean down, clicked;

		public Key() {
			keys[keyCounter] = this;
			keyCounter++;
		}

		public void press() {
			if (!down) {
				down = true;
				presses++;
				clicked = true;
			}
		}

		public void release() {
			down = false;
			clicked = false;
		}

		public void toggle(boolean pressed) {
			if(pressed)
				release();
			else
				press();
		}

		public void tick() {
			if(absorbs < presses) {
				absorbs++;
				clicked = true;
			} else {
				clicked = false;
			}
		}
	}

	public InputHandler(Game game) {
		this.game = game;
	}

	public Key[] keys = new Key[7];

	public Key up = new Key();
	public Key down = new Key();
	public Key left = new Key();
	public Key right = new Key();
	public Key attack = new Key();
	public Key menu = new Key();
	public Key pause = new Key();

	public void tick() {
		int keyStates = game.getKeyStates();

		if((keyStates & GameCanvas.UP_PRESSED) != 0) {
			up.press();
		} else {
			up.release();
		}

		if((keyStates & GameCanvas.DOWN_PRESSED) != 0) {
			down.press();
		} else {
			down.release();
		}

		if((keyStates & GameCanvas.LEFT_PRESSED) != 0) {
			left.press();
		} else {
			left.release();
		}

		if((keyStates & GameCanvas.RIGHT_PRESSED) != 0) {
			right.press();
		} else {
			right.release();
		}

		if((keyStates & GameCanvas.FIRE_PRESSED) != 0) {
			attack.press();
		} else {
			attack.release();
		}

		/*if((keyStates & GameCanvas.KEY_NUM9) != 0) {
			menu.press();
		} else {
			menu.release();
		}*/

		for(int i = 0; i < keys.length; i++) {
			keys[i].tick();
		}
	}

	public void toggle(int keyCode, boolean pressed) {
		if(keyCode == GameCanvas.KEY_STAR)
			menu.toggle(pressed);
	}
}