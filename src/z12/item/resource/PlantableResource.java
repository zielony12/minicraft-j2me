package z12.item.resource;

import java.util.Vector;

import z12.entity.Player;
import z12.level.Level;
import z12.level.tile.Tile;

public class PlantableResource extends Resource {
	private Vector sourceTiles;
	private Tile targetTile;

	public PlantableResource(String name, int sprite, int color, Tile targetTile, Tile[] sourceTiles) {
		super(name, sprite, color);
		this.targetTile = targetTile;

		//this.sourceTiles = sourceTiles;
		
		this.sourceTiles = new Vector();
		
		for(int i = 0; i < sourceTiles.length; i++) {
			this.sourceTiles.addElement(sourceTiles[i]);
		}
	}

	public boolean interactOn(Tile tile, Level level, int xt, int yt, Player player, int attackDir) {
		if (sourceTiles.contains(tile)) {
			level.setTile(xt, yt, targetTile, 0);
			return true;
		}
		return false;
	}
}
