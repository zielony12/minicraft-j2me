package z12;

import java.io.IOException;
import java.util.Random;

import javax.microedition.lcdui.*;
import javax.microedition.lcdui.game.*;

import z12.entity.Player;
import z12.gfx.Color;
import z12.gfx.Font;
import z12.gfx.Screen;
import z12.gfx.SpriteSheet;
import z12.level.Level;
import z12.level.tile.Tile;
import z12.screen.DeadMenu;
import z12.screen.LevelTransitionMenu;
import z12.screen.Menu;
import z12.screen.TitleMenu;
import z12.screen.WonMenu;

public class Game extends GameCanvas implements Runnable {
	public static final String VERSION = "0.1.2b";
	private Random random = new Random();
	public static final String NAME = "Minicraft";
	public static int HEIGHT;
	public static int WIDTH;
	//private static final int SCALE = 3;

	private int[] pixels;
	private boolean running = false;
	private Screen screen;
	private Screen lightScreen;
	private InputHandler input = new InputHandler(this);

	private int[] colors = new int[256];
	private int tickCount = 0;
	public int gameTime = 0;

	private Level level;
	private Level[] levels = new Level[5];
	private int currentLevel = 3;
	public Player player;

	public Menu menu;
	private int playerDeadTime;
	private int pendingLevelChange;
	private int wonTimer = 0;
	public boolean hasWon = false;
	public boolean started = false;

	public Display display;

	public Game(Display display) {
		super(false);

		this.display = display;

		setFullScreenMode(true);

		WIDTH = getWidth();
		HEIGHT = getHeight();
		pixels = new int[WIDTH * HEIGHT];
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
		if (menu != null) menu.init(this, input);
	}

	public void start() {
		running = true;
		new Thread(this).start();
	}

	public void stop() {
		running = false;
	}

	public void resetGame(int width, int height) {
		playerDeadTime = 0;
		wonTimer = 0;
		gameTime = 0;
		hasWon = false;

		levels = new Level[5];
		currentLevel = 3;

		/* Warning: default dimensions are 32x32. */
		levels[4] = new Level(width, height, 1, null);
		levels[3] = new Level(width, height, 0, levels[4]);
		levels[2] = new Level(width, height, -1, levels[3]);
		levels[1] = new Level(width, height, -2, levels[2]);
		levels[0] = new Level(width, height, -3, levels[1]);

		level = levels[currentLevel];
		player = new Player(this, input);
		player.findStartPos(level);

		level.add(player);

		for (int i = 0; i < 5; i++) {
			levels[i].trySpawn(5000);
		}

		started = true;
	}

	private void init() {
		int pp = 0;
		for (int r = 0; r < 6; r++) {
			for (int g = 0; g < 6; g++) {
				for (int b = 0; b < 6; b++) {
					int rr = (r * 255 / 5);
					int gg = (g * 255 / 5);
					int bb = (b * 255 / 5);
					int mid = (rr * 30 + gg * 59 + bb * 11) / 100;

					int r1 = ((rr + mid * 1) / 2) * 230 / 255 + 10;
					int g1 = ((gg + mid * 1) / 2) * 230 / 255 + 10;
					int b1 = ((bb + mid * 1) / 2) * 230 / 255 + 10;
					colors[pp++] = r1 << 16 | g1 << 8 | b1;

				}
			}
		}

		screen = new Screen(WIDTH, HEIGHT, new SpriteSheet("/icons.png"));
		lightScreen = new Screen(WIDTH, HEIGHT, new SpriteSheet("/icons.png"));

		setMenu(new TitleMenu());
	}

	public void run() {
		long lastTime = System.currentTimeMillis();
		double unprocessed = 0;
		double msPerTick = 1000.0 / 55;
		int frames = 0;
		int ticks = 0;
		long lastTimer1 = System.currentTimeMillis();

		init();

		while (running) {
			long now = System.currentTimeMillis();
			unprocessed += (now - lastTime) / msPerTick;
			lastTime = now;
			boolean shouldRender = true;
			
			while (unprocessed >= 1) {
				ticks++;
				tick();
				unprocessed -= 1;
				shouldRender = true;
			}

			try {
				Thread.sleep(2);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

			if (shouldRender) {
				frames++;
				render(getGraphics());
			}

			if (System.currentTimeMillis() - lastTimer1 > 1000) {
				lastTimer1 += 1000;
				System.out.println(ticks + " ticks, " + frames + " fps");
				frames = 0;
				ticks = 0;
			}
		}
	}

	public void tick() {
		tickCount++;
		//if (!hasFocus()) {
		//	input.releaseAll();
		//} else {
		if (started && !player.removed && !hasWon) gameTime++;

		input.tick();
		if (menu != null) {
			menu.tick();
		} else {
			if (player.removed) {
				playerDeadTime++;
				if (playerDeadTime > 60) {
					setMenu(new DeadMenu());
				}
			} else {
				if (pendingLevelChange != 0) {
					setMenu(new LevelTransitionMenu(pendingLevelChange));
					pendingLevelChange = 0;
				}
			}
			if (wonTimer > 0) {
				if (--wonTimer == 0) {
					setMenu(new WonMenu());
				}
			}
			level.tick();
			Tile.tickCount++;
		}
		//}
	}

	public void changeLevel(int dir) {
		level.remove(player);
		currentLevel += dir;
		level = levels[currentLevel];
		player.x = (player.x >> 4) * 16 + 8;
		player.y = (player.y >> 4) * 16 + 8;
		level.add(player);

	}

	public void render(Graphics g) {
		if(started) {
			int xScroll = player.x - screen.w / 2;
			int yScroll = player.y - (screen.h - 8) / 2;
			if (xScroll < 16) xScroll = 16;
			if (yScroll < 16) yScroll = 16;
			if (xScroll > level.w * 16 - screen.w - 16) xScroll = level.w * 16 - screen.w - 16;
			if (yScroll > level.h * 16 - screen.h - 16) yScroll = level.h * 16 - screen.h - 16;
			if (currentLevel > 3) {
				int col = Color.get(20, 20, 121, 121);
				for (int y = 0; y < 14; y++)
					for (int x = 0; x < 24; x++) {
						screen.render(x * 8 - ((xScroll / 4) & 7), y * 8 - ((yScroll / 4) & 7), 0, col, 0);
					}
			}

			level.renderBackground(screen, xScroll, yScroll);
			level.renderSprites(screen, xScroll, yScroll);

			if (currentLevel < 3) {
				lightScreen.clear(0);
				level.renderLight(lightScreen, xScroll, yScroll);
				screen.overlay(lightScreen, xScroll, yScroll);
			}
		}

		renderGui();

		for (int y = 0; y < screen.h; y++) {
			for (int x = 0; x < screen.w; x++) {
				int cc = screen.pixels[x + y * screen.w];
				if (cc < 255) pixels[x + y * WIDTH] = colors[cc];
			}
		}

		int ww = WIDTH * 3;
		int hh = HEIGHT * 3;
		int xo = (WIDTH - ww) / 2;
		int yo = (HEIGHT - hh) / 2;

		g.drawRGB(pixels, 0, WIDTH, 0, 0, WIDTH, HEIGHT, false);
		flushGraphics();
	}

	private void renderGui() {
		if (menu != null) {
			menu.render(screen);
			return;
		}

		for (int y = 0; y < 2; y++) {
			for (int x = 0; x < screen.w / 8; x++) {
				screen.render(x * 8, screen.h - 16 + y * 8, 0 + 12 * 32, Color.get(000, 000, 000, 000), 0);
			}
		}

		for (int i = 0; i < 10; i++) {
			if (i < player.health)
				screen.render(i * 8, screen.h - 16, 0 + 12 * 32, Color.get(000, 200, 500, 533), 0);
			else
				screen.render(i * 8, screen.h - 16, 0 + 12 * 32, Color.get(000, 100, 000, 000), 0);

			if (player.staminaRechargeDelay > 0) {
				if (player.staminaRechargeDelay / 4 % 2 == 0)
					screen.render(i * 8, screen.h - 8, 1 + 12 * 32, Color.get(000, 555, 000, 000), 0);
				else
					screen.render(i * 8, screen.h - 8, 1 + 12 * 32, Color.get(000, 110, 000, 000), 0);
			} else {
				if (i < player.stamina)
					screen.render(i * 8, screen.h - 8, 1 + 12 * 32, Color.get(000, 220, 550, 553), 0);
				else
					screen.render(i * 8, screen.h - 8, 1 + 12 * 32, Color.get(000, 110, 000, 000), 0);
			}
		}
		if (player.activeItem != null) {
			player.activeItem.renderInventory(screen, 10 * 8, screen.h - 16);
		}
	}

	public void scheduleLevelChange(int dir) {
		pendingLevelChange = dir;
	}

	public void won() {
		wonTimer = 60 * 3;
		hasWon = true;
	}

	/*
		keyPressed/Released - only for the asterisk (*) key
		which is inherited from Canvas and doesn't support
		GameCanvas.getKeyStates() method.
	*/ 

	protected void keyPressed(int key) {
		input.toggle(key, true);
	}

	protected void keyReleased(int key) {
		input.toggle(key, false);
	}

	public void cleanUp() {
		//todo: free up resources
		level = null;
		screen = null;
		input = null;
		menu = null;
	}
}