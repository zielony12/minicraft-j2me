package z12.screen;

import javax.microedition.lcdui.TextField;
import javax.microedition.lcdui.Command;
import javax.microedition.lcdui.Displayable;
import javax.microedition.lcdui.Form;
import javax.microedition.lcdui.Item;
import javax.microedition.lcdui.CommandListener;
import javax.microedition.lcdui.ChoiceGroup;
import javax.microedition.lcdui.Choice;

import z12.gfx.Color;
import z12.gfx.Font;
import z12.gfx.Screen;
import z12.sound.Sound;

public class WorldMenu extends Menu implements CommandListener {
	private int selected = 0;
	private int loadingWait = 10;
	private boolean loading = false;
	private Menu parent;

	private static int width = 32;
	private static int height = 32;

	private TextField worldWidthField = new TextField("World width", "", 4, TextField.PHONENUMBER);
	private TextField worldHeightField = new TextField("World height", "", 4, TextField.PHONENUMBER);
	private Form form = new Form("World options", new Item[] { worldWidthField, worldHeightField });

	private Command okCmd = new Command("OK", Command.OK, 0);
	private Command cancelCmd = new Command("Cancel", Command.EXIT, 1);

	private static String[] options = { "Create new", "World options", "Back" };

	public WorldMenu(Menu parent) {
		this.parent = parent;
		form.addCommand(okCmd);
		form.addCommand(cancelCmd);
		form.setCommandListener(this);
	}

	public void tick() {
		if(loading && loadingWait > 0)
			loadingWait--;
		
		if(loadingWait <= 0) {
			game.resetGame(width, height);
			game.setMenu(null);
		}

		if (input.up.clicked) selected--;
		if (input.down.clicked) selected++;

		int len = options.length;
		if (selected < 0) selected += len;
		if (selected >= len) selected -= len;

		if (input.attack.clicked || input.menu.clicked) {
			if (selected == 0) {
				loading = true;
				Sound.test.play();
			}
			if (selected == 1) game.display.setCurrent(form);
			if (selected == 2) game.setMenu(parent);
		}
	}

	public void render(Screen screen) {
		screen.clear(0);

		int h = 2;
		int w = 13;
		int titleColor = Color.get(0, 010, 131, 551);
		int xo = (screen.w - w * 8) / 2;
		int yo = 24;
		for (int y = 0; y < h; y++) {
			for (int x = 0; x < w; x++) {
				screen.render(xo + x * 8, yo + y * 8, x + (y + 6) * 32, titleColor, 0);
			}
		}

		for (int i = 0; i < options.length; i++) {
			String msg = options[i];
			int col = Color.get(0, 222, 222, 222);
			if (i == selected) {
				msg = "> " + msg + " <";
				col = Color.get(0, 555, 555, 555);
			}
			Font.draw(msg, screen, (screen.w - msg.length() * 8) / 2, (8 + i) * 8, col);
		}

		//TODO: progress bar
		if(loading) {
			screen.clear(0);
			String msg = "GENERATING NEW WORLD...";
			Font.draw(msg, screen, (screen.w - msg.length() * 8) / 2, 20, Color.get(0, 333, 333, 333));
			msg = "BE PATIENT!";
			Font.draw(msg, screen, (screen.w - msg.length() * 8) / 2, 28, Color.get(0, 333, 333, 333));
		}
	}

	public void commandAction(Command c, Displayable d) {
		if(c == okCmd) {
			width = Integer.parseInt(worldWidthField.getString());
			height = Integer.parseInt(worldHeightField.getString());
			game.display.setCurrent(game);
		}
		if(c == cancelCmd) {
			game.display.setCurrent(game);
		}
	}
}
