package z12.screen;

import z12.gfx.Screen;

public interface ListItem {
	void renderInventory(Screen screen, int i, int j);
}
