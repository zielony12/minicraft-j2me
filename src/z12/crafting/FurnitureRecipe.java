package z12.crafting;

import z12.entity.Furniture;
import z12.entity.Player;
import z12.entity.Anvil;
import z12.entity.Chest;
import z12.entity.Furnace;
import z12.entity.Lantern;
import z12.entity.Oven;
import z12.entity.Workbench;
import z12.item.FurnitureItem;
import z12.item.Item;

public class FurnitureRecipe extends Recipe {
	private Furniture furniture;

	public FurnitureRecipe(Furniture furniture) throws InstantiationException, IllegalAccessException {
		super(new FurnitureItem(furniture));
		this.furniture = furniture;
	}

	public void craft(Player player) {
		try {
			player.inventory.add(0, new FurnitureItem(furniture));
		} catch (Exception e) {
			//throw new RuntimeException(e);
			e.printStackTrace();
		}
	}
}
