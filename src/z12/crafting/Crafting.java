package z12.crafting;

import java.util.Vector;

import z12.entity.Anvil;
import z12.entity.Chest;
import z12.entity.Furnace;
import z12.entity.Oven;
import z12.entity.Lantern;
import z12.entity.Workbench;
import z12.item.ToolType;
import z12.item.resource.Resource;

public class Crafting {
	public static final Vector anvilRecipes = new Vector();
	public static final Vector ovenRecipes = new Vector();
	public static final Vector furnaceRecipes = new Vector();
	public static final Vector workbenchRecipes = new Vector();

	static {
		try {
			workbenchRecipes.addElement(new FurnitureRecipe(new Lantern()).addCost(Resource.wood, 5).addCost(Resource.slime, 10).addCost(Resource.glass, 4));

			workbenchRecipes.addElement(new FurnitureRecipe(new Oven()).addCost(Resource.stone, 15));
			workbenchRecipes.addElement(new FurnitureRecipe(new Furnace()).addCost(Resource.stone, 20));
			workbenchRecipes.addElement(new FurnitureRecipe(new Workbench()).addCost(Resource.wood, 20));
			workbenchRecipes.addElement(new FurnitureRecipe(new Chest()).addCost(Resource.wood, 20));
			workbenchRecipes.addElement(new FurnitureRecipe(new Anvil()).addCost(Resource.ironIngot, 5));

			workbenchRecipes.addElement(new ToolRecipe(ToolType.sword, 0).addCost(Resource.wood, 5));
			workbenchRecipes.addElement(new ToolRecipe(ToolType.axe, 0).addCost(Resource.wood, 5));
			workbenchRecipes.addElement(new ToolRecipe(ToolType.hoe, 0).addCost(Resource.wood, 5));
			workbenchRecipes.addElement(new ToolRecipe(ToolType.pickaxe, 0).addCost(Resource.wood, 5));
			workbenchRecipes.addElement(new ToolRecipe(ToolType.shovel, 0).addCost(Resource.wood, 5));
			workbenchRecipes.addElement(new ToolRecipe(ToolType.sword, 1).addCost(Resource.wood, 5).addCost(Resource.stone, 5));
			workbenchRecipes.addElement(new ToolRecipe(ToolType.axe, 1).addCost(Resource.wood, 5).addCost(Resource.stone, 5));
			workbenchRecipes.addElement(new ToolRecipe(ToolType.hoe, 1).addCost(Resource.wood, 5).addCost(Resource.stone, 5));
			workbenchRecipes.addElement(new ToolRecipe(ToolType.pickaxe, 1).addCost(Resource.wood, 5).addCost(Resource.stone, 5));
			workbenchRecipes.addElement(new ToolRecipe(ToolType.shovel, 1).addCost(Resource.wood, 5).addCost(Resource.stone, 5));

			anvilRecipes.addElement(new ToolRecipe(ToolType.sword, 2).addCost(Resource.wood, 5).addCost(Resource.ironIngot, 5));
			anvilRecipes.addElement(new ToolRecipe(ToolType.axe, 2).addCost(Resource.wood, 5).addCost(Resource.ironIngot, 5));
			anvilRecipes.addElement(new ToolRecipe(ToolType.hoe, 2).addCost(Resource.wood, 5).addCost(Resource.ironIngot, 5));
			anvilRecipes.addElement(new ToolRecipe(ToolType.pickaxe, 2).addCost(Resource.wood, 5).addCost(Resource.ironIngot, 5));
			anvilRecipes.addElement(new ToolRecipe(ToolType.shovel, 2).addCost(Resource.wood, 5).addCost(Resource.ironIngot, 5));

			anvilRecipes.addElement(new ToolRecipe(ToolType.sword, 3).addCost(Resource.wood, 5).addCost(Resource.goldIngot, 5));
			anvilRecipes.addElement(new ToolRecipe(ToolType.axe, 3).addCost(Resource.wood, 5).addCost(Resource.goldIngot, 5));
			anvilRecipes.addElement(new ToolRecipe(ToolType.hoe, 3).addCost(Resource.wood, 5).addCost(Resource.goldIngot, 5));
			anvilRecipes.addElement(new ToolRecipe(ToolType.pickaxe, 3).addCost(Resource.wood, 5).addCost(Resource.goldIngot, 5));
			anvilRecipes.addElement(new ToolRecipe(ToolType.shovel, 3).addCost(Resource.wood, 5).addCost(Resource.goldIngot, 5));

			anvilRecipes.addElement(new ToolRecipe(ToolType.sword, 4).addCost(Resource.wood, 5).addCost(Resource.gem, 50));
			anvilRecipes.addElement(new ToolRecipe(ToolType.axe, 4).addCost(Resource.wood, 5).addCost(Resource.gem, 50));
			anvilRecipes.addElement(new ToolRecipe(ToolType.hoe, 4).addCost(Resource.wood, 5).addCost(Resource.gem, 50));
			anvilRecipes.addElement(new ToolRecipe(ToolType.pickaxe, 4).addCost(Resource.wood, 5).addCost(Resource.gem, 50));
			anvilRecipes.addElement(new ToolRecipe(ToolType.shovel, 4).addCost(Resource.wood, 5).addCost(Resource.gem, 50));

			furnaceRecipes.addElement(new ResourceRecipe(Resource.ironIngot).addCost(Resource.ironOre, 4).addCost(Resource.coal, 1));
			furnaceRecipes.addElement(new ResourceRecipe(Resource.goldIngot).addCost(Resource.goldOre, 4).addCost(Resource.coal, 1));
			furnaceRecipes.addElement(new ResourceRecipe(Resource.glass).addCost(Resource.sand, 4).addCost(Resource.coal, 1));

			ovenRecipes.addElement(new ResourceRecipe(Resource.bread).addCost(Resource.wheat, 4));
		} catch (Exception e) {
			//throw new RuntimeException(e);
			e.printStackTrace();
		}
	}
}