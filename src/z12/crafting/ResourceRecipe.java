package z12.crafting;

import z12.entity.Player;
import z12.gfx.Color;
import z12.gfx.Font;
import z12.gfx.Screen;
import z12.item.ResourceItem;
import z12.item.resource.Resource;

public class ResourceRecipe extends Recipe {
	private Resource resource;

	public ResourceRecipe(Resource resource) {
		super(new ResourceItem(resource, 1));
		this.resource = resource;
	}

	public void craft(Player player) {
		player.inventory.add(0, new ResourceItem(resource, 1));
	}
}
