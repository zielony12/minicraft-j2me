package z12.gfx;

import javax.microedition.lcdui.Image;

import java.io.IOException;

public class SpriteSheet {
	public int width, height;
	public int[] pixels;

	public SpriteSheet(String name) {
		try {
			Image image = Image.createImage(name);
			width = image.getWidth();
			height = image.getHeight();
			pixels = new int[width * height];
			image.getRGB(pixels, 0, width, 0, 0, width, height);
			for(int i = 0; i < pixels.length; i++) {
				pixels[i] = (pixels[i] & 0xff) / 64;
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}