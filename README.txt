A port of Notch's minicraft to the ancient Java 2 MicroEdition platform.

Demo screenshot: SCREENSHOT.PNG
(Nokia N86 with S60 running minicraft-j2me through J2ME)

Build instructions:
Install Sun's Java Wireless Toolkit for CLDC:
<https://www.oracle.com/java/technologies/sun-java-wireless-toolkit.html>
Open the project and hit build.
---------
zielony12
